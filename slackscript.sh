
#!/bin/bash
set -euo pipefail
FAILURE=1
SUCCESS=0
PROJECT_SLUG="project-slug"
SLACKWEBHOOKURL="https://hooks.slack.com/services/XXXXXXXXXXX"
function build_slack_message() {
	local slack_msg_header
	# Populate header and define slack channels
	slack_msg_header=":rocket: *<${CI_PROJECT_TITLE}>*: New Commit in <${CI_PROJECT_URL}/-/tree/${CI_COMMIT_BRANCH}|${CI_COMMIT_BRANCH}> by ${GITLAB_USER_NAME}"
	
    cat <<-SLACK
{
    "channel": "slack-channel",
	"blocks": [
		{
			"type": "section",
			"text": {
				"type": "mrkdwn",
				"text": "${slack_msg_header}"
			}
		},
		{
			"type": "divider"
		},
		{
			"type": "section",
			"fields": [
                {
					"type": "mrkdwn",
					"text": "*Commit: [<${CI_PROJECT_URL}/-/commit/${CI_COMMIT_SHA}|${CI_COMMIT_SHORT_SHA}>]*\n${CI_COMMIT_TITLE} "
				},
                {
					"type": "mrkdwn",
					"text": "*Quality Report:*\n <https://<YOUR PROJECT SPACE>.gitlab.io/-/${PROJECT_SLUG}/-/jobs/${CI_JOB_ID}/artifacts/gl-code-quality-report.html|Download here>"
				}
				
			]
		},
		{
			"type": "divider"
		}
	]
}
	SLACK
}

curl -X POST --data-urlencode "payload=$(build_slack_message)" "${SLACKWEBHOOKURL}"
